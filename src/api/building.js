import request from '@/utils/request'
// 楼栋列表
export function getSpackBuildingList (parameter) {
  return request({
    url: '/space/building/list',
    method: 'get',
    params: parameter
  })
}
// 楼栋添加
export function addSpackBuilding (parameter) {
  return request({
    url: '/space/building/add',
    method: 'post',
    params: parameter
  })
}
// 楼栋修改
export function editSpackBuilding (parameter) {
  return request({
    url: '/space/building/edit',
    method: 'post',
    params: parameter
  })
}
// 楼栋删除
export function deleteSpackBuilding (parameter) {
  return request({
    url: '/space/building/delete',
    method: 'post',
    params: parameter
  })
}
// 楼栋详情
export function detailSpackBuilding (arampeter) {
  return request({
    url: '/space/building/detail',
    method: 'get',
    params: arampeter
  })
}
