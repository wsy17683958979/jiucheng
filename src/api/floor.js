import request from '@/utils/request'
// 楼层列表
export function getSpackFloorList (parameter) {
  return request({
    url: '/space/floor/list',
    method: 'get',
    params: parameter
  })
}
// 楼层添加
export function addSpackFloor (parameter) {
  return request({
    url: '/space/floor/add',
    method: 'post',
    data: parameter
  })
}
// 楼层修改
export function editSpackFloor (parameter) {
  return request({
    url: '/space/floor/edit',
    method: 'post',
    data: parameter
  })
}
// 楼层删除
export function deleteSpackFloor (parameter) {
  return request({
    url: '/space/floor/delete',
    method: 'post',
    data: parameter
  })
}
// 楼层查询
export function detailSpackFloor (parameter) {
  return request({
    url: '/space/floor/detail',
    method: 'post',
    params: parameter
  })
}

// id == 0 add     post
// id != 0 update  put
// export function saveService (parameter) {
//   return request({
//     url: api.service,
//     method: parameter.id === 0 ? 'post' : 'put',
//     data: parameter
//   })
// }

// export function saveSub (sub) {
//   return request({
//     url: '/sub',
//     method: sub.id === 0 ? 'post' : 'put',
//     data: sub
//   })
// }
