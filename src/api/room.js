import request from '@/utils/request'
// 房源列表
export function getSpaceRoomList (parameter) {
  return request({
    url: '/space/room/list',
    method: 'get',
    params: parameter
  })
}
// 房源添加
export function addSpaceRoom (parameter) {
  return request({
    url: '/space/room/add',
    method: 'post',
    params: parameter
  })
}
// 房源修改
export function editSpaceRoom (parameter) {
  return request({
    url: '/space/room/edit',
    method: 'post',
    params: parameter
  })
}
// 房源删除
export function deleteSpaceRoom (parameter) {
  return request({
    url: '/space/room/delete',
    method: 'post',
    params: parameter
  })
}
// 房源查询
export function detailSpaceRoom (parameter) {
  return request({
    url: '/space/room/detail',
    method: 'post',
    params: parameter
  })
}

// // id == 0 add     post
// // id != 0 update  put
// export function saveService (parameter) {
//   return request({
//     url: api.service,
//     method: parameter.id === 0 ? 'post' : 'put',
//     data: parameter
//   })
// }

// export function saveSub (sub) {
//   return request({
//     url: '/sub',
//     method: sub.id === 0 ? 'post' : 'put',
//     data: sub
//   })
// }
