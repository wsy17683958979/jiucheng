import request from '@/utils/request'
// const api = {
//   user: '/user',
//   role: '/role',
//   service: '/service',
//   permission: '/permission',
//   permissionNoPager: '/permission/no-pager',
//   orgTree: '/org/tree'
// }
// export default api
// 园区列表
export function getSpackParkList (parameter) {
  return request({
    url: '/space/park/list',
    method: 'get',
    params: parameter
  })
}
// 园区添加
export function addSpackPark (parameter) {
  return request({
    url: '/space/park/add',
    method: 'post',
    params: parameter
  })
}
// 园区修改
export function editSpackPark (parameter) {
  return request({
    url: '/space/park/edit',
    method: 'post',
    params: parameter
  })
}
// 园区删除
export function deleteSpackPark (parameter) {
  return request({
    url: '/space/park/delete',
    method: 'post',
    params: parameter
  })
}
// 园区详情
export function detailSparkPack (parameter) {
  return request({
    url: '/space/park/detail',
    method: 'post',
    params: parameter
  })
}

// id == 0 add     post
// id != 0 update  put
// export function saveService (parameter) {
//   return request({
//     url: api.service,
//     method: parameter.id === 0 ? 'post' : 'put',
//     data: parameter
//   })
// }

// export function saveSub (sub) {
//   return request({
//     url: '/sub',
//     method: sub.id === 0 ? 'post' : 'put',
//     data: sub
//   })
// }
